package com.lezione29.mobilificio.classi;

import java.util.ArrayList;

public class Mobile {
	private Integer idMobile;
	private String nome;
	private String codice;
	private String descrizione;
	private Float prezzo;
	private ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
	
	public Mobile(){
		
	}
	public void setIdMobile(Integer idMobile) {
		this.idMobile = idMobile;
	}
	public Integer getIdMobile() {
		return idMobile;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}
	
	public ArrayList<Categoria> getElencoCategorie() {
		return elencoCategorie;
	}
	public void setElencoCategorie(ArrayList<Categoria> elencoCategorie) {
		this.elencoCategorie = elencoCategorie;
	}
	@Override
	public String toString() {
		return "Mobile [idMobile=" + idMobile + ", nome=" + nome + ", codice=" + codice + ", descrizione=" + descrizione
				+ ", prezzo=" + prezzo + "]";
	}
	
	
}
