package com.lezione29.mobilificio.classi;

public class Categoria {
	private Integer idCategoria;
	private String nome;
	private String codice;
	private String descrizione;
	
	public Categoria(){
		
	}
	
	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}
	public Integer getIdCategoria() {
		return idCategoria;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Categoria [idCategoria=" + idCategoria + ", nome=" + nome + ", codice=" + codice + ", descrizione="
				+ descrizione + "]";
	}
	
	
}
