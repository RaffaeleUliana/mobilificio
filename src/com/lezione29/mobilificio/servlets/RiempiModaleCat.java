package com.lezione29.mobilificio.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.mobilificio.classi.Categoria;
import com.lezione29.mobilificio.classi.Mobile;
import com.lezione29.mobilificio.dao.MobileDAO;

/**
 * Servlet implementation class RecuperaMobili
 */
@WebServlet("/riempimodalecat")
public class RiempiModaleCat extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MobileDAO mobDAO = new MobileDAO();
		
		ArrayList<Categoria> elencoCategorie = new ArrayList<Categoria>();
		String codice = request.getParameter("key_codice");
		try {
			elencoCategorie = mobDAO.findCategorie(codice);
			Gson jsonizzatore= new Gson();
			
			String risultatoJson = jsonizzatore.toJson(elencoCategorie);
			PrintWriter out = response.getWriter();
			out.print(risultatoJson);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
