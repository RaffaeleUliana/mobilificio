package com.lezione29.mobilificio.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.mobilificio.classi.Categoria;
import com.lezione29.mobilificio.classi.Mobile;
import com.lezione29.mobilificio.connessione.ConnettoreDB;
import com.mysql.jdbc.PreparedStatement;

public class MobileDAO implements DAO{

	@Override
	public Object getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(Object t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<Mobile> findAll() throws SQLException {
		ArrayList<Mobile> elenco = new ArrayList<Mobile>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idMobile, nome, codice, descrizione, prezzo FROM mobile;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Mobile temp = new Mobile();
			temp.setIdMobile(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			temp.setPrezzo(risultato.getFloat(5));
			
			temp.setElencoCategorie(this.findCategorie(temp.getCodice()));
			elenco.add(temp);
		}
		
		return elenco;
	}
	
	public ArrayList<Categoria> findCategorie(String codiceMobile) throws SQLException {
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "";
		query = "SELECT c.idCategoria, c.nome, c.codice, c.descrizione "+
				"FROM categoria c "+
				"JOIN mobileCategoria ON mobileCategoria.idCategoria = c.idCategoria "+
				"JOIN mobile ON mobile.idMobile = mobileCategoria.idMobile "+
				"WHERE mobile.codice = ?;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, codiceMobile);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Categoria temp = new Categoria();
			temp.setIdCategoria(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			elenco.add(temp);
		}
		
		return elenco;
	}

}
