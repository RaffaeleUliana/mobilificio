package com.lezione29.mobilificio.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.mobilificio.classi.Categoria;
import com.lezione29.mobilificio.connessione.ConnettoreDB;
import com.mysql.jdbc.PreparedStatement;

public class CategoriaDAO implements DAO{
	public ArrayList<Categoria> findAll() throws SQLException{
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT idCategoria, nome, codice, descrizione FROM categoria;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Categoria temp = new Categoria();
			temp.setIdCategoria(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setDescrizione(risultato.getString(4));
			elenco.add(temp);
		}
		
		return elenco;
	}

	@Override
	public Object getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(Object t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Object t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	
}
