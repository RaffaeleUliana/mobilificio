package com.lezione29.mobilificio.dao;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO<T> {
	
	T getById(int id) throws SQLException;
	
	ArrayList<T> findAll() throws SQLException;
	
	void insert(T t) throws SQLException;
	
	boolean delete(T t) throws SQLException;
	
	boolean update(T t) throws SQLException;
}
