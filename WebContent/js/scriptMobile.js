function riempiTabella(arr_mobili){
	let result = '';
	for(let i =0; i <arr_mobili.length; i++){
		result += mobiliRisultato(arr_mobili[i]);
	}
	$("#contenuto").html(result);
}

function mobiliRisultato(obj_mobili){
	let result = '<tr data-codice-mobile = "'+obj_mobili.codice+'">';
	result += '<td>';
	result += obj_mobili.nome;
	result += '</td>';
	result += '<td>';
	result += obj_mobili.codice;
	result += '</td>';
	result += '<td>';
	result += obj_mobili.descrizione;
	result += '</td>';
	result += '<td>';
	result += '<a data-target="#myModal" data-toggle="modal" class="MainNavText" id="modalCategorie" href="#myModal" onclick = "apriCategorie(this)">Categorie</a>';
	result += '</td>';
	result += '<td>';
	result += obj_mobili.prezzo;
	result += '</td>';
	result += '</tr>';

	return result;
}


function riempiCategorie(arr_categorie){
	let result = '';
	for(let i =0; i <arr_categorie.length; i++){
		result += categorieRisultato(arr_categorie[i]);
	}
	$("#contenuto-modale").html(result);
}

function categorieRisultato(obj_categorie){
	let result = '<tr data-codice-mobile = "'+obj_categorie.codice+'">';
	result += '<td>';
	result += obj_categorie.nome;
	result += '</td>';
	result += '<td>';
	result += obj_categorie.codice;
	result += '</td>';
	result += '<td>';
	result += obj_categorie.descrizione;
	result += '</td>';
	result += '</tr>';

	return result;
}

function apriCategorie(link){
	let val_codice = $(link).parent().parent().data("codice-mobile")
	$.ajax(
					{
						url: "http://localhost:8080/Mobilificio/riempimodalecat",
						method: "GET",
						data: {"key_codice": val_codice},
						success: function(ris_success){
							console.log("Tutto OK");
							
							let risJson = JSON.parse(ris_success);
							riempiCategorie(risJson);
							console.log(risJson);
						},
						error: function(ris_error) {
							console.log("ERRORE");
							console.log(ris_error);
						}
					}
				);
}




$(document).ready(
	function() {
		$("#elencaMobili").click(
			function(){
				$.ajax(
					{
						url: "http://localhost:8080/Mobilificio/recuperamobili",
						method: "GET",
						success: function(ris_success){
							console.log("Tutto OK");
							
							let risJson = JSON.parse(ris_success);
							riempiTabella(risJson);
							console.log(risJson);
						},
						error: function(ris_error) {
							console.log("ERRORE");
							console.log(ris_error);
						}
					}
				);
			}
		);
		
		$("#modalCategorie").click(
			function(){
				$("myModal").modal(show);
				riempiModaleCategorie(myModal);
				
			}
		);
	}
);