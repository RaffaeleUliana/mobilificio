function riempiTabella(arr_categorie){
	let result = '';
	for(let i =0; i <arr_categorie.length; i++){
		result += categoriaRisultato(arr_categorie[i]);
	}
	$("#contenuto").html(result);
}

function categoriaRisultato(obj_categoria){
	let result = '<tr>';
	result += '<td>';
	result += obj_categoria.nome;
	result += '</td>';
	result += '<td>';
	result += obj_categoria.codice;
	result += '</td>';
	result += '<td>';
	result += obj_categoria.descrizione;
	result += '</td>';
	result += '</tr>';

	return result;
}



$(document).ready(
	function() {
		$("#elencaCategorie").click(
			function(){
				$.ajax(
					{
						url: "http://localhost:8080/Mobilificio/recuperacategorie",
						method: "GET",
						success: function(ris_success){
							console.log("Tutto OK");
							
							let risJson = JSON.parse(ris_success);
							riempiTabella(risJson);
							console.log(risJson);
						},
						error: function(ris_error) {
							console.log("ERRORE");
							console.log(ris_error);
						}
					}
				);
			}
		);
	}
);