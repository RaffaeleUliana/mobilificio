DROP DATABASE IF EXISTS lez_29_mobilificio;
CREATE DATABASE lez_29_mobilificio;
USE lez_29_mobilificio;

CREATE TABLE mobile(
	idMobile INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	nome VARCHAR(50) NOT NULL,
	descrizione TEXT,
	codice VARCHAR(20) UNIQUE NOT NULL,
	prezzo FLOAT DEFAULT(0)
);

CREATE TABLE categoria(
	idCategoria INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    descrizione TEXT,
    codice VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE mobileCategoria(
	idMobile INTEGER NOT NULL,
    idCategoria INTEGER NOT NULL,
    
    PRIMARY KEY(idMobile, idCategoria),
    FOREIGN KEY (idMobile) REFERENCES mobile(idMobile) ON DELETE CASCADE,
	FOREIGN KEY (idCategoria) REFERENCES categoria(idCategoria) ON DELETE CASCADE
    
);

INSERT INTO mobile (nome, codice, prezzo) VALUES
("cassapanca", "CS123456", 25),
("divano", "DV123456", 250),
("tavolo", "TV123456", 55);

INSERT INTO categoria (nome, codice) VALUES
("legno", "LG123456"),
("cucina", "CU123456"),
("camera", "CA123456"),
("relax", "RX123456");

INSERT INTO mobileCategoria (idMobile, idCategoria) VALUES
(1, 1),
(1, 3),
(2, 2),
(2, 4),
(3, 1),
(3, 2);

SELECT * FROM categoria;

SELECT c.idCategoria, c.nome, c.codice, c.descrizione
	FROM categoria c
    JOIN mobileCategoria ON mobileCategoria.idCategoria = c.idCategoria
    JOIN mobile ON mobile.idMobile = mobileCategoria.idMobile
    WHERE mobile.idMobile = 1;